build:
	# Build the image
	docker build -t rocker/caviarma .

start:
	# Starts a detached container
	docker run -td --name caviarma-dev \
		-v ${PWD}:/caviarma -w /caviarma rocker/caviarma 

attach: 
	# Enters the running container
	docker exec -it caviarma-dev /bin/bash
