#include "RcppArmadillo.h"
// [[Rcpp::depends(RcppArmadillo)]]

using namespace Rcpp;

//' The adaptive CAViaR model
//' Source : \url{http://www.simonemanganelli.org/Simone/Research.html}.
//'
//' @param THETA The quantile scalar value in (0,1).
//' @param K The k parameter described by Engle and Manganelli (2004).
//' @param BETA The parameter vector.
//' @param y The return vector.
//' @param empiricalQuantile The empirical quantile.
//' @export
// [[Rcpp::export]]
arma::vec ADAPTIVEloop(double THETA, double K, arma::vec BETA, arma::vec y,
                       double empiricalQuantile) {

  int RowsOfy       = y.n_elem;
  arma::vec VaR(RowsOfy);  // Filled with zeros.

  VaR(0) = empiricalQuantile;

  for(int i = 1; i < RowsOfy; i++) {

    VaR(i) = VaR(i-1) + BETA(0) * (1/(1 + exp(K*(y(i-1)+ VaR(i-1)))) - THETA);
    //VaR[i] = VaR[i-1] + BETA[0] * ((y[i-1]<-VaR[i-1]) - THETA);
  }

  return VaR;
}

//' The Asymetric CAViaR model
//' Source : \url{http://www.simonemanganelli.org/Simone/Research.html}.
//'
//' @param BETA The parameter vector.
//' @param y The return vector.
//' @param empiricalQuantile The empirical quantile.
//' @export
// [[Rcpp::export]]
arma::vec ASloop(arma::vec BETA, arma::vec y,
                 double empiricalQuantile) {

  int RowsOfy       = y.n_elem;
  arma::vec VaR(RowsOfy);  // Filled with zeros.

  VaR(0) = empiricalQuantile;

  for(int i = 1; i < RowsOfy; i++) {

    VaR(i) = BETA(0) + BETA(1) * VaR(i-1) + BETA(2) * y(i-1) * (y(i-1) > 0) -
      BETA(3) * y(i-1) * (y(i-1) < 0);

  }

  return VaR;
}

//' The Symetric Absolute Value CAViaR model
//' Source : \url{http://www.simonemanganelli.org/Simone/Research.html}.
//'
//' @param BETA The parameter vector.
//' @param y The return vector.
//' @param empiricalQuantile The empirical quantile.
//' @export
// [[Rcpp::export]]
arma::vec SAVloop(arma::vec BETA, arma::vec y, double empiricalQuantile) {

  int RowsOfy       = y.n_elem;
  arma::vec VaR(RowsOfy);  // Filled with zeros.

  VaR(0) = empiricalQuantile;

  for(int i = 1; i < RowsOfy; i++) {

    VaR(i) = BETA(0) + BETA(1) * VaR(i-1) + BETA(2) * (y(i-1)*(y(i-1)>0) -
      y(i-1)*(y(i-1)<0));

  }

  return VaR;
}

//' The GARCH-like CAViaR model
//' Source : \url{http://www.simonemanganelli.org/Simone/Research.html}.
//'
//' @param BETA The parameter vector.
//' @param y The return vector.
//' @param empiricalQuantile The empirical quantile.
//' @export
// [[Rcpp::export]]
arma::vec GARCHloop(arma::vec BETA, arma::vec y, double empiricalQuantile) {

  int RowsOfy       = y.n_elem;
  arma::vec VaR(RowsOfy);  // Filled with zeros.

  VaR(0) = empiricalQuantile;

  for(int i = 1; i < RowsOfy; i++) {

    VaR(i) =  sqrt(BETA(0) + BETA(1) * pow(VaR(i-1),2) + BETA(2) * pow(y(i-1),2));

  }

  return VaR;
}

//' The T-CAViaR model
//'
//' @param BETA The parameter vector.
//' @param y The return vector.
//' @param empiricalQuantile The empirical quantile.
//' @export
// [[Rcpp::export]]
arma::vec TCAViaRloop(arma::vec BETA, arma::vec y, double empiricalQuantile) {

  int RowsOfy       = y.n_elem;
  arma::vec VaR(RowsOfy);  // Filled with zeros.

  VaR(0) = empiricalQuantile;

  for(int i = 1; i < RowsOfy; i++) {

    if (y[i-1]<=0) {
      VaR(i) = BETA(0) + BETA(1) * VaR(i-1) + BETA(2) * (y(i-1)*(y(i-1)>0) -
        y(i-1)*(y(i-1)<0));
    } else {
      VaR(i) = BETA(3) + BETA(4) * VaR(i-1) + BETA(5) * (y(i-1)*(y(i-1)>0) -
        y(i-1)*(y(i-1)<0));
    }

  }

  return VaR;
}
