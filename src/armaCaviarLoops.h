#ifndef CAVIARLOOPS_H
#define CAVIARLOOPS_H

#include "RcppArmadillo.h"
// [[Rcpp::depends(RcppArmadillo)]]

using namespace Rcpp;

arma::vec ADAPTIVEloop(double THETA, double K, arma::vec BETA,
                           arma::vec y, double empiricalQuantile);

arma::vec ASloop(arma::vec BETA, arma::vec y,
                     double empiricalQuantile);

arma::vec SAVloop(arma::vec BETA, arma::vec y,
                      double empiricalQuantile);

arma::vec GARCHloop(arma::vec BETA, arma::vec y,
                        double empiricalQuantile);

arma::vec TCAViaRloop(arma::vec BETA, arma::vec y,
                          double empiricalQuantile);

#endif
