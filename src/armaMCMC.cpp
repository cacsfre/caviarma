#include "RcppArmadillo.h"
//#include <boost/progress.hpp>
// [[Rcpp::depends(RcppArmadillo)]]
// [[Rcpp::depends(BH)]]

#include "armaCaviarLoops.h"

using namespace Rcpp;

static arma::vec y;
static double alpha, caviar_model, empirical_quantile;
static arma::rowvec test_means;
static arma::mat test_sigma;
static bool test_mode = false;

//' Sets the model data
//'
//' @param thisY The data vector.
//' @param thisModel The CAViaR model.
//' @param thisAlpha The quantile level..
//' @param empQuantile The empirical quantile.
//' @export
// [[Rcpp::export]]
void SetModel(const arma::vec thisY, double thisModel, double thisAlpha,
              double empQuantile) {

  y = thisY;
  caviar_model = thisModel;
  alpha = thisAlpha;
  empirical_quantile = empQuantile;

  test_mode = false;

}

double armaBetaPosterior(arma::vec beta) {
  int Tsize = y.n_elem;
  arma::vec VaR(Tsize, arma::fill::zeros); VaR(0) = empirical_quantile;
  arma::vec alphaVec(Tsize); alphaVec.fill(alpha);

  if (caviar_model == 1) {  // Symmetric Absolute Value.
    VaR = SAVloop(beta, y, VaR(0));
  }

  if (caviar_model == 2) {  // Asymmetric Slope.
    VaR = ASloop(beta, y, VaR(0));
    // Hit = (y < -VaR) - alpha;
  }

  if (caviar_model == 3) {  // GARCH.

   VaR = GARCHloop(beta, y, VaR(0));
    // Hit = (y < -VaR) - alpha;
  }

  if (caviar_model == 4) {  // Adaptive.
    int K = 10;      // If K is large, the optimisation algorithm for the
    // adaptive caviar_model has to be changed, because the objective
    // function becomes discontinuous. An alternative is the Genetic Algorithm.
    VaR = ADAPTIVEloop(alpha, K, beta, y, VaR(0));
    // Hit = (y < -VaR) - alpha;
  }

  if (caviar_model == 5) {  // T-CAViaR.
    VaR = TCAViaRloop(beta, y, VaR(0));
  }

  // Posterior

  arma::vec D_vec = (y-VaR) % (alphaVec - (y < VaR));

  double D = accu(D_vec) - D_vec(0);

  double beta_posterior = (-Tsize)*log(D);  // Equation (11) in Gerlach

  return beta_posterior;  // Returning beta|y
}


// Source : \url{https://gallery.rcpp.org/articles/dmvnorm_arma/}
// Licensed under GPL-2
static double const log2pi = std::log(2.0 * M_PI);

arma::vec dmvnrm_arma(arma::mat const &x,
                      arma::rowvec const &mean,
                      arma::mat const &sigma,
                      bool const logd = false) {
    using arma::uword;
    uword const n = x.n_rows,
             xdim = x.n_cols;
    arma::vec out(n);
    arma::mat const rooti = arma::inv(trimatu(arma::chol(sigma)));
    double const rootisum = arma::sum(log(rooti.diag())),
                constants = -(double)xdim/2.0 * log2pi,
              other_terms = rootisum + constants;

    arma::rowvec z;
    for (uword i = 0; i < n; i++) {
        z      = (x.row(i) - mean) * rooti;
        out(i) = other_terms - 0.5 * arma::dot(z, z);
    }

    if (logd)
      return out;
    return exp(out);
}

void SetTest(arma::rowvec const &mean, arma::mat const &sigma) {
  test_means = mean;
  test_sigma = sigma;
  test_mode = true;
}

double arma_p_log(arma::vec x) {
  arma::vec p_log = dmvnrm_arma(arma::reshape(x, 1, x.n_elem),
                                test_means, test_sigma, true);
  return p_log[0];
}

//' Adaptive MCMC sampler
//'
//' @param theta_seed The parameter vector's initial values.
//' @param nsim The simulation size.
//' @param eta A scalar value between 1/2 and 1.
//' @param adapt_frequency An integer value greater than 1.
//' @param init_scale A double value equal to 1/sd².
//' @param proposal_dist A string in ("normal", "student")
//' @export
// [[Rcpp::export]]
arma::mat AMCMC(arma::vec theta_seed, int nsim, double eta = 0.6,
                int adapt_frequency = 60, double init_scale = 1,
                std::string proposal_dist = "normal") {

//  boost::progress_display show_progress(nsim);

  int theta_length = theta_seed.n_elem;
  int burn_in = floor(nsim/3);

  // Proposal parameters
  //  double init_scale = 1;
  arma::vec scale(theta_length); scale.fill(init_scale);
  //  arma::mat S = arma::eye(theta_length, theta_length); S.diag() = scale;
  arma::vec proposal_draws(theta_length);
  int  nu = 5;

  // Atchade
  //double eta = 0.5;
  double lower_bound = 1e-10;
  double upper_bound = 10;

  // Data
  arma::uvec accept_counter(theta_length, arma::fill::zeros);
  arma::uvec burn_in_counter(theta_length, arma::fill::zeros);

  arma::mat x(nsim, theta_length, arma::fill::zeros);  // Markov chain
  arma::vec Y(theta_length); // Proposal

  x.row(0) = theta_seed.t();
  double q_x = 1.0;
  double q_y = 1.0;

  for (int i = 1; i < burn_in; i++) { // Burn-in

    x.row(i) = x.row(i-1);

    if (proposal_dist == "student") {
      proposal_draws = rt(theta_length, nu);
    } else if (proposal_dist == "normal") {
      proposal_draws = arma::randn(theta_length);
    }

    for (int j = 0; j < theta_length; j++) {  // Gibbs-like

      Y = x.row(i).t();
      Y(j) = Y(j) + sqrt(scale(j))*proposal_draws(j);

      double f_x = armaBetaPosterior(x.row(i).t());
      double f_y = armaBetaPosterior(Y);

      double prob = exp(f_y + q_x - f_x - q_y);
      double rho = std::min(1.0, prob);

      bool accept = false;

      if (arma::randu() < rho) {
        x.row(i) = Y.t();
        accept = true;
      } else {
        accept = false;
      }

      if(accept) {
        accept_counter(j) = accept_counter(j) + 1;
        burn_in_counter(j) = burn_in_counter(j) + 1;
      }

      if(i%adapt_frequency == 0) {
        //scale(j) = scale(j) + ((double)(accept_counter(j)/adapt_frequency) - 0.36)/
        //  ((double)pow(i / adapt_frequency, eta));
        scale(j) = scale(j) + ((double)(burn_in_counter(j)/i) - 0.36)/
          ((double)pow(i, eta));
        if(scale(j) < lower_bound) scale(j) = lower_bound;
        if(scale(j) > upper_bound) scale(j) = upper_bound;
        //accept_counter(j) = 0;
      }
    }

//    ++show_progress;

  }  // End of Burn-in

  arma::vec burn_in_mu(theta_length);
  arma::mat burn_in_sigma(theta_length, theta_length);

  burn_in_mu = arma::mean(x.rows(burn_in/3, burn_in), 0).t();  // colMean
  burn_in_sigma = arma::cov(x.rows(burn_in/3, burn_in));

  int sampling_counter = 0;

  for (int i = burn_in; i < nsim; i++) { // Sampling

    x.row(i) = x.row(i-1);

    Y = arma::mvnrnd(burn_in_mu, burn_in_sigma, 1);

    // Kernel
    double g_x = 1.0;
    double g_y = 1.0;

    double f_x = armaBetaPosterior(x.row(i).t());
    double f_y = armaBetaPosterior(Y);

    double prob = exp(f_y + g_x - f_x - g_y);
    double rho = std::min(1.0, prob);

    if (arma::randu() < rho) {
      x.row(i) = Y.t();
      sampling_counter ++;
    } else {

    }

//    ++show_progress;

  }  // End of Sampling

  return x;

}


//' Robust Adaptive MCMC sampler
//' Source : \url{https://github.com/scheidan/adaptMCMC}
//'
//' @param nsim The simulation size.
//' @param theta_seed The parameter vector's initial values.
//' @param init_scale A double value equal to 1/sd².
//' @param acc_rate The target acceptance rate.
//' @param gamma A scalar value between 1/2 and 1 (adaptive MCMC algorithm).
//' @param adapt Boolean, whether the algorithm should use adaptation.
//' @param n_start Observation where the sampling starts, zero means from the beginning.
//' @export
// [[Rcpp::export]]
arma::mat robustAMCMC(int nsim, arma::vec theta_seed, arma::vec init_scale,
    double acc_rate = 0, double gamma = 2/3, bool adapt = false, int n_start = 0) {

//  boost::progress_display show_progress(nsim);

  int theta_d = theta_seed.n_elem;
  int nu = theta_d;

  // Number of adaption steps
  double n_adapt = 0;  // Adapt is false by default

  if(!(acc_rate == 0)) {  // @TODO: Use extra variable for custom n_adapt ?
    adapt = true;
    n_adapt = std::numeric_limits<double>::infinity();
  }

  int ii;  // Counter for adapt step
  double adapt_rate;
  arma::mat I_mat = arma::eye(theta_d, theta_d);
  arma::cx_vec eig_val;
  const double double_eps = std::numeric_limits<double>::epsilon();
  double tol;

  arma::mat x(nsim, theta_d, arma::fill::zeros);  // Markov chain
  arma::vec U(theta_d);                           // Proposal pre-draw
  arma::vec Y(theta_d);                           // Proposal

  // Initial S
  arma::mat M = arma::eye(theta_d, theta_d);
  M.diag() = init_scale;

  arma::mat S = arma::chol(M).t();

  x.row(0) = theta_seed.t();

  // Vector to store log densities p(x)
  arma::vec p_val(nsim); p_val.fill(0);

  double p_val_prop;

  p_val_prop = test_mode ? arma_p_log(x.row(0).t()) : armaBetaPosterior(x.row(0).t());

  p_val(0) = p_val_prop;

  int k = 0;  // Accept counter

  for (int i = 1; i < nsim; i++) { // Sampling

    U = rt(theta_d, nu);
    Y = x.row(i - 1).t() + (S * U);  // Proposal value

    p_val_prop = test_mode ? arma_p_log(Y) : armaBetaPosterior(Y);  // Density at proposed value

    double prob = exp(p_val_prop - p_val(i - 1));
    double alpha = std::min(1.0, prob);  // Rho probability in AMCMC

    if (!std::isfinite(alpha)) alpha = 0;  // If zero divided by zero

    if (arma::randu() < alpha) {
      x.row(i) = Y.t();  // Accept
      p_val(i) = p_val_prop;
      k++;
    } else {
      x.row(i) = x.row(i - 1);  // Or not
      p_val(i) = p_val(i - 1);
    }

    // Compute new S
    ii = i + n_start;

    if (ii < n_adapt) {
      adapt_rate = std::min(1.0, theta_d * std::pow(ii, -gamma));

      M = S * (I_mat + adapt_rate * (alpha - acc_rate) * U * U.t() /
               arma::accu(arma::pow(U, 2))) * S.t();

      // Check if M is positive definite. If not, use nearPD().
      eig_val = arma::eig_gen(M);
      tol = M.n_cols * arma::max(arma::abs(eig_val)) * double_eps;

      // Should use M.is_symmetric OR M.is_hermitian ?
      if (!M.is_sympd() || !arma::imag(eig_val).is_zero() ||
          !arma::all(arma::real(eig_val) > tol)) {

        //@TODO: compute nearest positive definite matrix
        // adaptMCMC uses https://github.com/cran/Matrix/blob/master/R/nearPD.R
        // See https://github.com/bstewart/stm/blob/master/src/STMCfuns.cpp
        // See https://stackoverflow.com/questions/51490499/results-for-calculating-nearest-positive-definite-matrix-are-different-in-r-func

      }

      S = arma::chol(M).t();  // Should fail if not positive definite

    }

//    ++show_progress;

  }  // End of Sampling

  Rcpp::Rcout << nsim << std::endl;

  return x;

}

/* R
y <- caviarma::gerlach[, "US"]
nsim <- 1e5
alpha <- 0.05
caviar_model <- 5

empirical_quantile <- quantile(y, alpha)
theta_seed <- runif(6, min = -2, max = 2)

SetModel(y, caviar_model, alpha, empirical_quantile)
last_mcmc_output <- robustAMCMC(theta_seed = theta_seed, init_scal = rep(1, length(theta_seed)), nsim = nsim, y = y, acc_rate = 0.234)
*/
