FROM rocker/shiny

# System dependencies
RUN apt-get update && apt-get install -y libpq-dev libxml2

# R development packages
RUN R -e "install.packages(c('devtools'))"

